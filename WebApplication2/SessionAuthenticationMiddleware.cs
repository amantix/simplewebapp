﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApplication2
{
    public class SessionAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public SessionAuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            string[] whiteList = { "/login", "/signin" };
            if (whiteList.Contains(context.Request.Path.Value) || context.Session.Keys.Contains("name"))
                _next.Invoke(context);
            else
                context.Response.Redirect("/login");
        }
    }
}
